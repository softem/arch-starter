import { DataBinder } from "@archjs/databinder/databinder";


var userListData = [];
for( var i =0; i< 10000; i++ ){
    userListData[i] = {
         id:  i
        ,username: 'test@test'+i+'.com'
        ,info: {
              firstName: 'John'+i
            , lastName: 'Doe'+i
            , age: 10+i
        }
    };
}

var model = DataBinder.createRecursiveBinder( userListData );
for( let key in model['_data'] ){
    var div     = document.createElement("div");
    var content = document.createTextNode( "[" + model[key]['id'] +  "] -> Hi" + model[key]['username'] );
    div.appendChild( content );
    let element = document.querySelector('#app').insertBefore(div,null);
    model[key].bind('username',function (data) {
        element.innerText = "[" + model[key]['id'] +  "] -> Hi" + model[key]['username'];
    });
    model.bind( key, function () {
        element.remove();
    });
}


window["model"] = model;
