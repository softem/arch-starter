const path              = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
var CopyWebpackPlugin   = require('copy-webpack-plugin');
var fs                  = require('fs-extra');

var output = (process.env.npm_config_env_env === 'production' )?'dist':'dev';

var externals     = [];
var webpackPlugin = [];
var packageJSON   = JSON.parse( fs.readFileSync('package.json') );
if( !process.env.npm_config_env_boundle ) {
    for (var key in packageJSON.dependencies) {
        externals.push(RegExp('^(' + key + '\/.*|\$)$', 'i'));//  packageJSON.dependencies;
        webpackPlugin.push({from: 'node_modules/' + key + '/', to: key});
    }
    output += '/amd';
}else{
    output += '/boundle';
}



module.exports = {
    entry:{
        main: './src/main.ts'
    },
    output: {
        path: path.resolve(__dirname, output),
        filename: '[name].js',
        libraryTarget: "amd"
    },

    // Currently we need to add '.ts' to the resolve.extensions array.
    resolve: {
        modules: [
            'node_modules'
        ],
        extensions: ['.ts', '.tsx', '.js', '.jsx' ]
    },

    // Source maps support ('inline-source-map' also works)
    devtool: 'source-map',

    // Add the loader for .ts files.
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader?tsconfig=amd-tsconfig.json&emitRequireType=false"
            }
        ]
    },

    //Use external (amd) dependencies, ignoring specified modules @boundle
    externals: externals, //[ /^(rxjs\/.*|\$)$/i ],

    //Copy amd modules
    plugins: [
        CopyWebpackPlugin( webpackPlugin ),
        CheckerPlugin
    ]
};
